

FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=target/*.jar
COPY target/*.jar adresse-service.jar

ENTRYPOINT ["java","-jar","/adresse-service.jar"]